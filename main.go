package main

import (
	"os"

	"gitlab.com/kittiza/xTunnel/app"
)

func main() {
	file, err := os.Open("config.json")
	if err != nil {
		panic(err)

	}
	xtn := app.New(file)
	xtn.SetSSH(app.SSH{
		Address:  "x:22",
		Username: "root",
		Password: "x@x",
	})

	sx, err := xtn.ConnectSSH()
	if err != nil {
		xtn.Log("ConnectSSH error: %s\n", err)

		return
	}
	xtn.Listen(sx)
}
