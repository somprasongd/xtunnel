package app

import (
	"io"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh"
)

//Tunnel เปิด Tunnel และสร้างการเชื่อมต่อ
func (x *XTunnel) Tunnel(sx *ssh.Client, name, serverListen, localRemote string) {
	sv, err := sx.Listen("tcp", serverListen)
	if err != nil {
		x.Log("[%s] SSH Listen error: %s\n", name, err)

		return
	}

	x.Log("[%s] SSH Listen: On %s\n", name, sv.Addr())

	x.Listens = append(x.Listens, listens{
		Name:     name,
		Listener: sv,
	})
	defer sv.Close()

	for {
		svconn, err := sv.Accept()
		if err != nil {
			// x.Log("[%s] sv Accept error: %s\n", name, err)
			continue
		}
		x.Log("[%s] Accept: %s\n", name, svconn.RemoteAddr())
		loconn, err := net.Dial("tcp", localRemote)
		if err != nil {
			x.Log("[%s] loconn Dial error: %s\n", name, err)
			continue
		}

		x.Log("[%s] loconn LocalAddr: %s\n", name, loconn.LocalAddr())
		x.Log("[%s] loconn RemoteAddr: %s\n", name, loconn.RemoteAddr())

		copyConn := func(writer, reader net.Conn) {
			_, err := io.Copy(writer, reader)
			if err != nil {
				x.Log("[%s] Copy: %s", name, err)
			}
			writer.Close()
			reader.Close()
		}

		go copyConn(loconn, svconn)
		go copyConn(svconn, loconn)

	}
}

//TunnelAll TunnelAll
func (x *XTunnel) TunnelAll(sx *ssh.Client) {
	for _, v := range x.Config {
		go func(sx *ssh.Client, v config) {
			ss, _ := sx.NewSession()
			ss.Run("kill -9 $(lsof -t -i:" + strings.Split(v.ServerLisen, ":")[1] + ")")
			go x.Tunnel(sx, v.Name, v.ServerLisen, v.LocalRemote)
		}(sx, v)

	}
}

//Listen Listen
func (x *XTunnel) Listen(sx *ssh.Client) {
	x.TunnelAll(sx)
	go func(sx *ssh.Client) {
		for {
			d := net.Dialer{Timeout: 5 * time.Second}

			_, err := d.Dial("tcp", x.SSH.Address)
			if err != nil {
				x.Log("Disconnect err:%s", err)
				sx, _ = x.ConnectSSH()
				x.Log("%s", "Reconnect.")
			}
			time.Sleep(5 * time.Second)
		}

	}(sx)
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)
	func(sx *ssh.Client, c chan os.Signal) {
		<-c
		x.Shuttdow(sx)

		os.Exit(0)
	}(sx, sigc)
}

//Shuttdow Shuttdow
func (x *XTunnel) Shuttdow(sx *ssh.Client) {

	for _, v := range x.Listens {

		v.Listener.Close()
		x.Log("[%s] shutting dow.", v.Name)
	}
	x.Listens = make([]listens, 0)
	x.Log("Caught signal : xTunnel shutting down.")
	sx.Close()
}
