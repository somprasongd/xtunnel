package app

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"os"
)

type (
	//XTunnel XTunnel struct
	XTunnel struct {
		SSH
		log     *log.Logger
		Config  []config
		Listens []listens
	}
	listens struct {
		Name     string
		Listener net.Listener
	}
	config struct {
		Name        string `json:"name"`
		ServerLisen string `json:"server_listen"`
		LocalRemote string `json:"local_remote"`
	}
)

//New สร้าง XTunnel
func New(config *os.File) *XTunnel {
	x := &XTunnel{
		log:     log.New(os.Stderr, "[xTunnel] ", log.LstdFlags),
		Listens: make([]listens, 0),
	}
	byteValue, err := ioutil.ReadAll(config)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(byteValue, &x.Config)
	if err != nil {
		panic(err)
	}
	return x
}
