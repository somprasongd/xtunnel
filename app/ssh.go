package app

import (
	"golang.org/x/crypto/ssh"
)

type (
	//SSH SSH struct
	SSH struct {
		Address  string
		Username string
		Password string
	}
)

//SetSSH กำหนดการเชื่อมต่อ ssh
func (x *XTunnel) SetSSH(ssh SSH) {
	x.SSH = ssh
}

//ConnectSSH เชื่อต่อไปยัง ssh
func (x *XTunnel) ConnectSSH() (*ssh.Client, error) {
	serverConn, err := ssh.Dial(
		"tcp",
		x.SSH.Address,
		&ssh.ClientConfig{
			User: x.SSH.Username,
			Auth: []ssh.AuthMethod{
				ssh.Password(x.SSH.Password),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		})
	if err != nil {
		return nil, err
	}
	x.Log("ConnectSSH > RemoteAddr: %s\n", serverConn.RemoteAddr())
	x.Log("ConnectSSH > LocalAddr: %s\n", serverConn.LocalAddr())
	x.Log("ConnectSSH > ServerVersion: %s\n", serverConn.ServerVersion())
	x.Log("ConnectSSH > ClientVersion: %s\n", serverConn.ClientVersion())

	return serverConn, nil

}
